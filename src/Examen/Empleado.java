package Examen;

import java.util.Calendar;

public class Empleado {

    public boolean igual(Empleado empleado) {
        boolean igual;
        igual = this.getNombre().equals(empleado.getNombre()) &&
                this.getSalario() == empleado.getSalario() &&
                this.getAnioAlta() == empleado.getAnioAlta();
        return igual;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @param salario the salario to set
     */
    public void setSalario(float salario) {
        this.salario = salario;
    }

    /**
     * @return the anioAlta
     */
    public int getAnioAlta() {
        return anioAlta;
    }

    /**
     * @param anioAlta the anioAlta to set
     */
    public void setAnioAlta(int anioAlta) {
        this.anioAlta = anioAlta;
    }

    /**
     * @return the estado
     */
    public EstadoEmpleado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(EstadoEmpleado estado) {
        this.estado = estado;
    }

               public enum EstadoEmpleado {ACTIVO, EXCEDENCIA}
    
	public static final int INICIO_EMPRESA = 2014;
	private String nombre;
	private float salario; 
	private int anioAlta;
	private EstadoEmpleado estado;
		
		
	public Empleado(String nombre, float salario, int anyoAlta) throws IllegalArgumentException{
		if (nombre == null || nombre.equals(""))
			throw new IllegalArgumentException("No se puede crear el empleado. "+
												"El nombre no es significativo.");	
				
		if (salario<=0)
			throw new IllegalArgumentException("No se puede crear el empleado. "+
												"El salario tiene que ser positivo.");
			
		if (!esCorrectoAnyo(anyoAlta))
			throw new IllegalArgumentException("No se puede crear el empleado. "+
												"El a�o de alta no es v�lido.");
		
		this.nombre = nombre;
		this.salario = salario;
		setAnyoAlta(anyoAlta);
		estado = EstadoEmpleado.ACTIVO;
	}
	
	
		
	private boolean esCorrectoAnyo(int anio){
		int anyoActual = Calendar.getInstance().get(Calendar.YEAR);
		if (anio >= INICIO_EMPRESA && anio <= anyoActual)
			return true;
		else return false;
	}
	
	private void setAnyoAlta(int anyoAlta) throws IllegalArgumentException{
	  if (!esCorrectoAnyo(anyoAlta)) 
		throw new IllegalArgumentException("El a�o que se quiere establecer no es v�lido");

	  this.setAnioAlta(anyoAlta);
	}
	
		
	public float getSalario() {
		if (getEstado() == EstadoEmpleado.ACTIVO)
			return salario;
		else 
		               return 0f;
		
	}

	public void incSalario(float subida) throws IllegalStateException{
		if (getEstado() == EstadoEmpleado.EXCEDENCIA)
			throw new IllegalStateException("No se puede incrementar el salario de un" +
											"empleado en excedencia");
		setSalario(getSalario() + subida); 
	}
	
		
              
	public boolean equals(Object obj) {
		
		boolean igual=false;
            
		if (obj == null) return false;
		
		
		if (this == obj) return true;
		
		
		Empleado empleado = (Empleado) obj;
		
		igual = igual(empleado);
                
                              return igual;
	}
	
	
	
}
